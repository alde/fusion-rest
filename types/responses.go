package types

// CreateResponse is returned along with a 201 status code
type CreateResponse struct {
	Type    string `json:"type"`
	ID      int64  `json:"id"`
	Message string `json:"message"`
}
