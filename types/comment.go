package types

// Comment holds the JSON structure of a Comment
type Comment struct {
	CommentID     int64  `json:"id"`
	CommentTypeID int64  `json:"comment_type_id"`
	CommentType   string `json:"comment_type"`
	Name          string `json:"name"`
	Message       string `json:"message"`
	Datestamp     string `json:"datestamp"`
	IP            string `json:"ip_address"`
}

// Comments is a collection of sComment structs
type Comments []Comment

// CreateComment is a helper to create a Comment Structure
func CreateComment(commentID, commentTypeID int64, commentType, name, message, datestamp, ip string) Comment {
	return Comment{
		CommentID:     commentID,
		CommentTypeID: commentTypeID,
		CommentType:   commentType,
		Name:          name,
		Message:       message,
		Datestamp:     datestamp,
		IP:            ip,
	}
}
