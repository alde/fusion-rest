package types

// NewsItem represents a row in the database
type NewsItem struct {
	NewsID        int    `json:"news_id"`
	UserID        int    `json:"user_id"`
	Reads         int    `json:"reads"`
	Subject       string `json:"subject"`
	Content       string `json:"content"`
	Created       string `json:"created"`
	AllowComments string `json:"allow_comments"`
	UserName      string `json:"user_name"`
}

// NewsItems is a collection of NewsItem
type NewsItems []NewsItem

// CreateNewsItem helps create a NewsItem object
func CreateNewsItem(newsID int, userID int, reads int, subject string, content string, created string, allowComments string, userName string) NewsItem {
	return NewsItem{
		NewsID:        newsID,
		UserID:        userID,
		Reads:         reads,
		Subject:       subject,
		Content:       content,
		Created:       created,
		AllowComments: allowComments,
		UserName:      userName,
	}
}

// PostNewsItem represents a HTTP POST json used to create a NewsItem
type PostNewsItem struct {
	UserID   int    `json:"user_id"`
	Subject  string `json:"subject"`
	Content  string `json:"content"`
	Category int    `json:"news_category"`
}
