package database

import (
	"database/sql"
	"fmt"
	"log"

	// Import SQL
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
)

// GetConnection returns a connection to the database
func GetConnection() *sql.DB {
	username := viper.GetString("username")
	password := viper.GetString("password")
	host := viper.GetString("host")
	port := viper.GetInt("port")
	database := viper.GetString("database")

	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", username, password, host, port, database)

	db, err := sql.Open("mysql", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	return db
}
