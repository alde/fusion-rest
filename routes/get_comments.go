package routes

import (
	"net/http"
	"strconv"

	"github.com/alde/fusion-rest/database"
	"github.com/alde/fusion-rest/types"
	"github.com/gorilla/mux"
)

// CommentsShow gets comments for a NewsItem
func CommentsShow(w http.ResponseWriter, r *http.Request) {
	db := database.GetConnection()
	defer db.Close()

	vars := mux.Vars(r)
	id, _ := strconv.ParseInt(vars["newsId"], 10, 64)
	sql := `SELECT comment_id, comment_name, comment_message, comment_datestamp, comment_ip
            FROM fusion_comment
            WHERE comment_item_id=? AND comment_type="N"`
	rows, err := db.Query(sql, id)
	defer rows.Close()
	handle(err)

	var comments types.Comments

	for rows.Next() {
		var commentID int
		var name, message, datestamp, ip string

		rows.Scan(&commentID, &name, &message, &datestamp, &ip)
		comment := types.CreateComment(int64(commentID), id, "N", name, message, datestamp, ip)

		comments = append(comments, comment)
	}

	writeJSON(w, comments)
}
