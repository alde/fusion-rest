package routes

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/alde/fusion-rest/database"
	"github.com/alde/fusion-rest/types"
	"github.com/gorilla/mux"
)

// AddComment is used to POST a comment item to the database
func AddComment(w http.ResponseWriter, r *http.Request) {
	var pci types.Comment

	vars := mux.Vars(r)

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	handle(err)

	if err := json.Unmarshal(body, &pci); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	pci.CommentTypeID, _ = strconv.ParseInt(vars["newsId"], 10, 64)

	n := insertComment(pci)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(n); err != nil {
		panic(err)
	}
}

func insertComment(pci types.Comment) types.CreateResponse {
	db := database.GetConnection()
	defer db.Close()

	sql := `INSERT INTO fusion_comment (comment_type_id, comment_type, comment_name, comment_message, comment_datestamp, comment_ip)
					VALUES(?, ?, ?, ?, ?, ?)`

	datestamp := int32(time.Now().Unix())
	resp, err := db.Exec(sql, pci.CommentTypeID, "N", pci.Name, pci.Message, datestamp, pci.IP)
	handle(err)

	newID, _ := resp.LastInsertId()

	return types.CreateResponse{
		Type:    "news",
		ID:      newID,
		Message: "Created",
	}
}
