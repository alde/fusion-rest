package routes

import (
	"net/http"
	"strconv"

	"github.com/alde/fusion-rest/database"
	"github.com/alde/fusion-rest/types"
	"github.com/gorilla/mux"
)

// NewsIndex renders all news
func NewsIndex(w http.ResponseWriter, r *http.Request) {
	db := database.GetConnection()
	defer db.Close()
	sql := `SELECT news_id, news_subject, news_news, news_datestamp, news_allow_comments, user_id, user_name, news_reads FROM fusion_news tn
            LEFT JOIN fusion_users tu ON tn.news_name=tu.user_id
            LEFT JOIN fusion_news_cats tc ON tn.news_cat=tc.news_cat_id
            WHERE news_draft='0'
            ORDER BY news_sticky DESC, news_datestamp DESC`
	rows, err := db.Query(sql)
	defer rows.Close()
	handle(err)

	var nims types.NewsItems

	for rows.Next() {
		var newsID, userID, reads int
		var subject, content, userName, created, allowComments string

		rows.Scan(&newsID, &subject, &content, &created, &allowComments, &userID, &userName, &reads)
		ni := types.CreateNewsItem(newsID, userID, reads, subject, content, created, allowComments, userName)

		nims = append(nims, ni)
	}

	writeJSON(w, nims)
}

// NewsShow shows a single news item
func NewsShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.ParseInt(vars["newsId"], 10, 64)

	ni := getNewsItem(id)

	writeJSON(w, ni)
}

func getNewsItem(id int64) types.NewsItem {
	db := database.GetConnection()
	defer db.Close()

	sql := `SELECT news_id, news_subject, news_news, news_datestamp, news_allow_comments, user_id, user_name, news_reads FROM fusion_news tn
            LEFT JOIN fusion_users tu ON tn.news_name=tu.user_id
            LEFT JOIN fusion_news_cats tc ON tn.news_cat=tc.news_cat_id
            WHERE news_draft='0' AND news_id=?`

	rows, err := db.Query(sql, id)
	defer rows.Close()
	handle(err)

	var ni types.NewsItem

	for rows.Next() {
		var newsID, userID, reads int
		var subject, content, userName, created, allowComments string

		rows.Scan(&newsID, &subject, &content, &created, &allowComments, &userID, &userName, &reads)
		ni = types.CreateNewsItem(newsID, userID, reads, subject, content, created, allowComments, userName)
	}

	return ni
}
