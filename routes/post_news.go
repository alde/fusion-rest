package routes

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/alde/fusion-rest/database"
	"github.com/alde/fusion-rest/types"
)

// AddNews is used to POST a news item to the database
func AddNews(w http.ResponseWriter, r *http.Request) {
	var pni types.PostNewsItem

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	handle(err)

	if err := json.Unmarshal(body, &pni); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	n := insertNews(pni)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(n); err != nil {
		panic(err)
	}
}

func insertNews(pni types.PostNewsItem) types.CreateResponse {
	db := database.GetConnection()
	defer db.Close()

	sql := `INSERT INTO fusion_news (news_subject, news_cat, news_news, news_name, news_datestamp)
					VALUES(?, ?, ?, ?, ?)`

	datestamp := int32(time.Now().Unix())
	resp, err := db.Exec(sql, pni.Subject, pni.Category, pni.Content, pni.UserID, datestamp)
	handle(err)

	newID, _ := resp.LastInsertId()

	return types.CreateResponse{
		Type:    "news",
		ID:      newID,
		Message: "Created",
	}
}
