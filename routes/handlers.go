package routes

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	// SQL Drivers
	_ "github.com/go-sql-driver/mysql"
)

// Index Rernder index route
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello")
}

func writeJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(data)
}

func handle(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
