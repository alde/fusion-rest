package routes

import (
	"net/http"

	"github.com/gorilla/mux"
)

// Route enforces the structure of a route
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes is a collection of route structs
type Routes []Route

// Create is used to create a new router
func Create() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"NewsIndex",
		"GET",
		"/news",
		NewsIndex,
	},
	Route{
		"NewsShow",
		"GET",
		"/news/{newsId}",
		NewsShow,
	},
	Route{
		"CommentsShow",
		"GET",
		"/news/{newsId}/comments",
		CommentsShow,
	},
	Route{
		"AddNews",
		"POST",
		"/news",
		AddNews,
	},
	Route{
		"AddComment",
		"POST",
		"/news/{newsId}/comments",
		AddComment,
	},
}
