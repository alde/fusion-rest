package main

import (
	"log"
	"net/http"

	"github.com/alde/fusion-rest/routes"
	"github.com/kardianos/osext"
	"github.com/spf13/viper"
)

func readConfig() {
	path, _ := osext.ExecutableFolder()
	override := "/etc/fusion-rest/"
	viper.AddConfigPath(override)
	viper.AddConfigPath(path)
	viper.SetConfigName("config")
	viper.ReadInConfig()
}

func main() {
	readConfig()
	router := routes.Create()

	log.Print("Listening to :8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
